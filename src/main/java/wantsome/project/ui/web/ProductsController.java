package wantsome.project.ui.web;

import io.javalin.http.Context;
import wantsome.project.db.products.dao.Filters;
import wantsome.project.db.products.dao.ProductsDAO;
import wantsome.project.db.products.dto.Brand;
import wantsome.project.db.products.dto.ProductType;
import wantsome.project.db.products.dto.ProductsDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsController {
    public static void allProductsPage(Context ctx) {
        String searchPattern = ctx.queryParam("searchPattern");
        String brandStr = ctx.queryParam("brand");
        String priceMinStr = ctx.queryParam("priceMin");
        String priceMaxStr = ctx.queryParam("priceMax");
        List<ProductsDTO> productsDTOList;

        if (searchPattern != null) {
            double priceMin = 0;
            double priceMax = 25000;
            productsDTOList = ProductsDAO.getProductsByNamePattern(searchPattern);
            Map<String, Object> model = new HashMap<>();
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);

        } else if (brandStr != null) {
            double priceMin = Double.parseDouble(priceMinStr);
            double priceMax = Double.parseDouble(priceMaxStr);
            Brand brand = Brand.valueOf(brandStr);
            productsDTOList = ProductsDAO.getAllFiltered(new Filters(brand, priceMin, priceMax));
            Map<String, Object> model = new HashMap<>();
            model.put("brand", brand);
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);

        } else if (priceMinStr != null && priceMaxStr != null) {
            double priceMin = Double.parseDouble(priceMinStr);
            double priceMax = Double.parseDouble(priceMaxStr);
            productsDTOList = ProductsDAO.getAllFiltered(new Filters(priceMin, priceMax));
            Map<String, Object> model = new HashMap<>();
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);

        } else {
            double priceMin = 0;
            double priceMax = 25000;
            productsDTOList = ProductsDAO.getAll();
            Map<String, Object> model = new HashMap<>();
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);
        }
    }

    public static void selectivePage(ProductType productType, Context ctx) {
        String searchPattern = ctx.queryParam("searchPattern");
        String brandStr = ctx.queryParam("brand");
        String priceMinStr = ctx.queryParam("priceMin");
        String priceMaxStr = ctx.queryParam("priceMax");
        List<ProductsDTO> productsDTOList;

        if (searchPattern != null) {
            double priceMin = 0;
            double priceMax = 25000;
            productsDTOList = ProductsDAO.getProductsByNamePattern(searchPattern);
            Map<String, Object> model = new HashMap<>();
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);

        } else if (brandStr != null) {
            double priceMin = Double.parseDouble(priceMinStr);
            double priceMax = Double.parseDouble(priceMaxStr);
            Brand brand = Brand.valueOf(brandStr);
            productsDTOList = ProductsDAO.getAllFiltered(new Filters(brand, productType, priceMin, priceMax));
            Map<String, Object> model = new HashMap<>();
            model.put("brand", brand);
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);

        } else if (priceMinStr != null && priceMaxStr != null) {
            double priceMin = Double.parseDouble(priceMinStr);
            double priceMax = Double.parseDouble(priceMaxStr);
            productsDTOList = ProductsDAO.getAllFiltered(new Filters(productType, priceMin, priceMax));
            Map<String, Object> model = new HashMap<>();
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            model.put("products", productsDTOList);
            ctx.render("products.vm", model);

        } else {
            double priceMin = 0;
            double priceMax = 25000;
            productsDTOList = ProductsDAO.getAllOfType(productType);
            Map<String, Object> model = new HashMap<>();
            model.put("products", productsDTOList);
            model.put("priceMin", priceMin);
            model.put("priceMax", priceMax);
            ctx.render("products.vm", model);
        }
    }
}
