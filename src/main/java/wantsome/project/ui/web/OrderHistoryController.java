package wantsome.project.ui.web;

import io.javalin.http.Context;
import wantsome.project.db.invoice.dao.OrderDetailsDAO;
import wantsome.project.db.invoice.dao.OrderItemDetailsDAO;
import wantsome.project.db.invoice.dto.OrderDetailsDTO;
import wantsome.project.db.invoice.dto.OrderItemDetailsDTO;
import wantsome.project.db.order.dao.OrdersDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderHistoryController {
    public static void orderHistoryPage(Context ctx) {
        long orderId = ctx.queryParamAsClass("orderId", Long.class).getOrDefault(-1L);
        List<Long> orderIdList = OrdersDAO.loadOrderIdListByClientId(LogInController.getSessionKeyClientId(ctx));
        Map<String, Object> model = new HashMap<>();
        if (orderId != -1) {
            OrderDetailsDTO orderDetails = OrderDetailsDAO.getOrderDetails(orderId, LogInController.getSessionKeyClientId(ctx)).orElseThrow();
            List<OrderItemDetailsDTO> itemsDetails = OrderItemDetailsDAO.getItemsDetails(orderId, LogInController.getSessionKeyClientId(ctx));
            model.put("orderDetails", orderDetails);
            model.put("orderItemDetails", itemsDetails);
        } else if (orderId == -1) {
            model.put("orderId", orderId);
        }
        model.put("orderIdList", orderIdList);
        ctx.render("orderhistory.vm", model);
    }
}
