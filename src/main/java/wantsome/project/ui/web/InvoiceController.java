package wantsome.project.ui.web;

import io.javalin.http.Context;
import wantsome.project.db.invoice.dao.OrderDetailsDAO;
import wantsome.project.db.invoice.dao.OrderItemDetailsDAO;
import wantsome.project.db.invoice.dto.OrderDetailsDTO;
import wantsome.project.db.invoice.dto.OrderItemDetailsDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InvoiceController {

    public static void invoicePage(Context ctx) {
        OrderDetailsDTO orderDetails = OrderDetailsDAO.getOrderDetails(OrdersController.getOrderID(ctx), LogInController.getSessionKeyClientId(ctx)).orElseThrow();
        List<OrderItemDetailsDTO> orderItemDetails = OrderItemDetailsDAO.getItemsDetails(OrdersController.getOrderID(ctx), LogInController.getSessionKeyClientId(ctx));
        Map<String, Object> model = new HashMap<>();
        model.put("orderDetails", orderDetails);
        model.put("orderItemDetails", orderItemDetails);
        ctx.render("invoice.vm", model);
    }
}
