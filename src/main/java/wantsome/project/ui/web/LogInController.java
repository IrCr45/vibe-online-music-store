package wantsome.project.ui.web;

import io.javalin.http.Context;
import wantsome.project.db.clients.dao.ClientsDAO;
import wantsome.project.db.clients.dto.ClientsDTO;

import java.util.HashMap;
import java.util.Map;

public class LogInController {

    private static final String SESSION_KEY_CLIENT_ID = "clientId";
    private static final String SESSION_KEY_EMAIL = "email";
    private static final String SESSION_KEY_PASSWORD = "password";
    private static final String SESSION_KEY_NO_MATCH = "noMatch";
    private static final String SESSION_KEY_USER_LOGGED_IN = "userLoggedIn";

    public static long getSessionKeyClientId(Context ctx) {
        return ctx.sessionAttribute(SESSION_KEY_CLIENT_ID);
    }

    public static String getSessionKeyEmail(Context ctx) {
        return ctx.sessionAttribute(SESSION_KEY_EMAIL);
    }

    public static String getSessionKeyPassword(Context ctx) {
        return ctx.sessionAttribute(SESSION_KEY_PASSWORD);
    }

    public static boolean getSessionKeyNoMatch(Context ctx) {
        return ctx.sessionAttribute("noMatch") != null ? ctx.sessionAttribute(SESSION_KEY_NO_MATCH) : false;
    }

    public static boolean getSessionKeyUserLoggedIn(Context ctx) {
        return ctx.sessionAttribute("userLoggedIn") != null ? ctx.sessionAttribute(SESSION_KEY_USER_LOGGED_IN) : false;
    }

    public static void setSessionKeyClientId(Context ctx, long id) {
        ctx.sessionAttribute(SESSION_KEY_CLIENT_ID, id);
    }

    public static void setSessionKeyEmail(Context ctx, String email) {
        ctx.sessionAttribute(SESSION_KEY_EMAIL, email);
    }

    public static void setSessionKeyPassword(Context ctx, String password) {
        ctx.sessionAttribute(SESSION_KEY_EMAIL, password);
    }

    public static void setSessionKeyNoMatch(Context ctx, boolean noMatch) {
        ctx.sessionAttribute(SESSION_KEY_NO_MATCH, noMatch);
    }

    public static void setSessionKeyUserLoggedIn(Context ctx, boolean userLoggedIn) {
        ctx.sessionAttribute(SESSION_KEY_USER_LOGGED_IN, userLoggedIn);
    }


    public static void signUpFormPage(Context ctx) {
        String name = ctx.formParam("name");
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        String street = ctx.formParam("street");
        int streetNumber = ctx.formParamAsClass("streetNumber", Integer.class).getOrDefault(-1);
        String town = ctx.formParam("town");
        ClientsDAO.insert(new ClientsDTO(email, password, name, street, streetNumber, town));
        ctx.redirect("/");
    }

    public static void logInValidate(Context ctx) {
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        long id = ClientsDAO.getIDByEmailAndPassword(email, password);
        setSessionKeyClientId(ctx, id);
        if (id == -1) {
            setSessionKeyNoMatch(ctx, true);
            ctx.redirect("/");
        } else {
            setSessionKeyNoMatch(ctx, false);
            setSessionKeyEmail(ctx, email);
            setSessionKeyUserLoggedIn(ctx, true);
            setSessionKeyPassword(ctx, password);
            ctx.redirect("/home");
        }
    }

    public static void logInPage(Context ctx) {
        boolean noMatch = getSessionKeyNoMatch(ctx);
        Map<String, Object> model = new HashMap<>();
        model.put("noMatch", noMatch);
        ctx.render("login.vm", model);
    }

    private static boolean pageAccessRestrict(String path) {
        return path.startsWith("/home") || path.startsWith("/products") || path.startsWith("/piano") || path.startsWith("/guitar")
                || path.startsWith("/drums") || path.startsWith("/violin") || path.startsWith("/viola") || path.startsWith("/cello")
                || path.startsWith("/cart") || path.startsWith("/ordercompleted") || path.startsWith("/orderhistory") || path.startsWith("/invoice");
    }

    public static void checkUserPresentOnSession(Context ctx) {
        if (pageAccessRestrict(ctx.path()) && !getSessionKeyUserLoggedIn(ctx)) {
            ctx.redirect("/");
        }
    }
}
