package wantsome.project.ui.web;

import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;
import wantsome.project.db.clients.dao.ClientsDAO;
import wantsome.project.db.clients.dto.ClientsDTO;
import wantsome.project.db.order.dao.CartDAO;
import wantsome.project.db.order.dao.OrdersDAO;
import wantsome.project.db.order.dto.CartDTO;
import wantsome.project.db.order.dto.CartItem;
import wantsome.project.db.order.dto.OrdersDTO;
import wantsome.project.db.products.dao.ProductsDAO;

import java.sql.Date;
import java.util.List;

public class OrdersController {

    private static final String SESSION_KEY_ORDER_ID = "orderId";

    public static long getOrderID(Context ctx) {
        try {
            long orderId = ctx.sessionAttribute(SESSION_KEY_ORDER_ID);
            return orderId != 0 ? orderId : -1;
        } catch (NullPointerException ex) {
            throw new RuntimeException("OrderID is null");
        }
    }

    public static void setOrderID(Context ctx, long id) {
        ctx.sessionAttribute(SESSION_KEY_ORDER_ID, id);
    }

    public static void dataGatheringAndInserting(Context ctx) {
        String street = ctx.formParam("street");
        int streetNumber = ctx.formParamAsClass("streetNumber", Integer.class).getOrDefault(-1);
        String town = ctx.formParam("town");

        long clientId = LogInController.getSessionKeyClientId(ctx);
        ClientsDTO client = ClientsDAO.load(clientId).orElseThrow();

        if (!street.equals(client.getStreet()) || streetNumber != client.getStreetNumber() || !town.equals(client.getTown())) {
            ClientsDAO.updateClientAddressDetails(clientId, street, streetNumber, town);
        }

        List<CartItem> cartItems = CartController.getCartItems(ctx);

        updateProductsStock(cartItems);

        OrdersDTO order = insertOrder(client, cartItems);

        insertCartItems(cartItems, order);

        OrdersController.setOrderID(ctx, order.getId());

        CartController.emptyCartItemsList(ctx);

        ctx.redirect("/ordercompleted");

    }

    private static void updateProductsStock(List<CartItem> cartItems) {
        for (CartItem cartItem : cartItems) {
            long productID = cartItem.getProduct().getId();
            int productStock = cartItem.getProduct().getStock();
            int quantity = cartItem.getQuantity();
            ProductsDAO.updateStock(productID, productStock - quantity);
        }
    }

    private static void insertCartItems(List<CartItem> cartItems, OrdersDTO order) {
        for (CartItem cartItem : cartItems) {
            CartDTO item = new CartDTO(cartItem.getProduct().getId(), order.getId(), cartItem.getQuantity());
            CartDAO.insert(item);
        }
    }

    @NotNull
    private static OrdersDTO insertOrder(ClientsDTO client, List<CartItem> cartItems) {
        double totalPrice = getTotalPrice(cartItems);
        OrdersDTO order = new OrdersDTO(client, new Date(System.currentTimeMillis()), totalPrice);
        OrdersDAO.insert(order);
        return order;
    }


    public static double getTotalPrice(List<CartItem> cartItems) {
        double totalPrice = 0;
        for (CartItem cartItem : cartItems) {
            int quantity = cartItem.getQuantity();
            double price = cartItem.getProduct().getPrice();
            totalPrice += price * quantity;
        }
        return totalPrice;
    }
}
