package wantsome.project.ui.web;

import io.javalin.http.Context;
import wantsome.project.db.clients.dao.ClientsDAO;
import wantsome.project.db.clients.dto.ClientsDTO;
import wantsome.project.db.order.dto.CartItem;
import wantsome.project.db.products.dao.ProductsDAO;
import wantsome.project.db.products.dto.ProductsDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartController {

    private static final String SESSION_KEY_CART_ITEMS = "cartItems";

    public static List<CartItem> getCartItems(Context ctx) {
        List<CartItem> cartItems = ctx.sessionAttribute(SESSION_KEY_CART_ITEMS);
        return cartItems != null ? cartItems : new ArrayList<>();
    }

    public static void emptyCartItemsList(Context ctx) {
        ctx.sessionAttribute(SESSION_KEY_CART_ITEMS, null);
    }

    public static void addToCart(Context ctx) {
        long productID = ctx.formParamAsClass("productID", Long.class).get();
        int quantity = ctx.formParamAsClass("quantity", Integer.class).get();
        ProductsDTO product = ProductsDAO.load(productID).orElseThrow();

        List<CartItem> cartItems = getCartItems(ctx);
        boolean notPresent = true;
        for (CartItem cartItem : cartItems) {
            if (cartItem.getProduct().getId() == productID) {
                cartItem.setQuantity(cartItem.getQuantity() + quantity);
                notPresent = false;
            }
        }
        if (notPresent) {
            CartItem cartItem = new CartItem(product, quantity);
            cartItems.add(cartItem);
        }
        ctx.sessionAttribute(SESSION_KEY_CART_ITEMS, cartItems);
        ctx.redirect("/cart");
    }

    public static void cartPage(Context ctx) {
        long productID = ctx.queryParamAsClass("productID", Long.class).getOrDefault(-1L);

        ClientsDTO client = ClientsDAO.load(LogInController.getSessionKeyClientId(ctx)).orElseThrow();

        if (productID != -1L) {
            List<CartItem> cartItems = getCartItems(ctx);
            cartItems.removeIf(cartItem -> cartItem.getProduct().getId() == productID);
            ctx.sessionAttribute(SESSION_KEY_CART_ITEMS, cartItems);
        }

        double totalPrice = OrdersController.getTotalPrice(getCartItems(ctx));

        Map<String, Object> model = new HashMap<>();
        model.put("client", client);
        model.put("totalPrice", totalPrice);
        model.put("cartItems", getCartItems(ctx));
        ctx.render("cart.vm", model);
    }
}
