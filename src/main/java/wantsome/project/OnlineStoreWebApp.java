package wantsome.project;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wantsome.project.db.products.dto.ProductType;
import wantsome.project.db.service.DBManager;
import wantsome.project.db.service.DbInit;
import wantsome.project.ui.web.*;

import static wantsome.project.ui.web.ProductsController.selectivePage;

/**
 * Example of main class for a web application (using Javalin framework)
 */
public class OnlineStoreWebApp {

    private static final Logger logger = LoggerFactory.getLogger(OnlineStoreWebApp.class);

    public static void main(String[] args) {
        setup();
        startWebServer();
    }

    private static void setup() {
        DBManager.setDbFile("OnlineStore.db");
        DbInit.initDatabase();
    }

    private static void startWebServer() {
        Javalin app = Javalin.create(config -> {
            config.addStaticFiles("/public", Location.CLASSPATH);
            config.enableDevLogging();
        }).start();

        /* LOGGING */
        app.before(LogInController::checkUserPresentOnSession);
        app.get("/", LogInController::logInPage);
        app.post("/", LogInController::logInValidate);
        app.get("/signup", ctx -> ctx.render("signup.vm"));
        app.post("/signup", LogInController::signUpFormPage);

        /* PRODUCTS */
        app.get("/home", ctx -> ctx.render("main.vm"));
        app.get("/products", ProductsController::allProductsPage);
        app.get("/piano", ctx -> selectivePage(ProductType.PIANO, ctx));
        app.get("/guitar", ctx -> selectivePage(ProductType.GUITAR, ctx));
        app.get("/drums", ctx -> selectivePage(ProductType.DRUMS, ctx));
        app.get("/violin", ctx -> selectivePage(ProductType.VIOLIN, ctx));
        app.get("/viola", ctx -> selectivePage(ProductType.VIOLA, ctx));
        app.get("/cello", ctx -> selectivePage(ProductType.CELLO, ctx));


        /* CART */
        app.get("/cart", CartController::cartPage);
        app.post("/products", CartController::addToCart);
        app.post("/cart", OrdersController::dataGatheringAndInserting);

        /* ORDER COMPLETED */

        app.get("/ordercompleted", ctx -> ctx.render("ordercompleted.vm"));

        /* INVOICE */

        app.get("/invoice", InvoiceController::invoicePage);

        /* ORDER HISTORY */

        app.get("/orderhistory", OrderHistoryController::orderHistoryPage);

        /* EXCEPTIONS */

        app.exception(Exception.class, (e, ctx) -> {
            logger.error("Unexpected exception", e);
            ctx.html("Unexpected exception: " + e);
        });
    }
}


