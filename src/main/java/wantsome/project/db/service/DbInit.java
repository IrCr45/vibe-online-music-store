package wantsome.project.db.service;

import wantsome.project.db.products.dao.ProductsDAO;
import wantsome.project.db.products.dto.Brand;
import wantsome.project.db.products.dto.ProductType;
import wantsome.project.db.products.dto.ProductsDTO;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.db.products.dto.Brand.*;
import static wantsome.project.db.products.dto.ProductType.*;

public class DbInit {

    private static final String CREATE_CLIENTS_SQL = "create table if not exists clients(\n" +
            "id integer primary key autoincrement,\n" +
            "email varchar(30) unique not null,\n" +
            "name varchar(100) not null,\n" +
            "password varchar(16) not null,\n" +
            "street varchar(100) not null,\n" +
            "streetNumber numeric not null,\n" +
            "town varchar(50) not null\n" +
            ");";
    private static final String CREATE_ORDERS_SQL = "create table if not exists orders(\n" +
            "id integer primary key autoincrement,\n" +
            "client_id integer references clients(id),\n" +
            "fulfill_date date,\n" +
            "total_price numeric\n" +
            ");";
    private static final String CREATE_PRODUCTS_SQL = "create table if not exists products(\n" +
            "id integer primary key autoincrement,\n" +
            "productType text check ( productType in ('" + PIANO + "','" + GUITAR + "','" + DRUMS + "','" + VIOLIN + "','" + VIOLA + "','" + CELLO + "')) not null,\n" +
            "description varchar(200) unique not null,\n" +
            "price numeric not null,\n" +
            "stock numeric not null,\n" +
            "image_path varchar(100) not null,\n" +
            "brand text check ( brand in ('" + CASIO + "','" + KORG + "','" + YAMAHA + "','" + DEXIBELL + "','" + IBANEZ + "','" + FREEDOM + "','" + TAKAMINE + "','" + TAMA + "','" + VOX + "','" + VHIENNA + "','" + ANTONI + "','" + FLAME + "','" + GEWA + "')) not null\n" +
            ");";
    private static final String CREATE_CART_SQL = "create table if not exists cart(\n" +
            "product_id integer references products(id),\n" +
            "order_id integer references orders(id),\n" +
            "quantity numeric \n" +
            ");";

    private static void createMissingTables() {
        try (Connection connection = DBManager.getConnection();
             Statement st = connection.createStatement()) {

            st.execute(CREATE_CLIENTS_SQL);
            st.execute(CREATE_ORDERS_SQL);
            st.execute(CREATE_PRODUCTS_SQL);
            st.execute(CREATE_CART_SQL);

        } catch (SQLException e) {
            throw new RuntimeException("Error creating missing tables: " + e.getMessage());
        }
    }

    private static void sampleProductList() {
        if (ProductsDAO.getAll().isEmpty()) {
            ProductsDAO.insert(new ProductsDTO(1, ProductType.PIANO, "Casio PX-770 Privia White", 3500, 0, "casio-px-770-we-pian-digital.jpg", CASIO));
            ProductsDAO.insert(new ProductsDTO(2, ProductType.PIANO, "Casio CDP-S350 Black", 2500, 12, "casio-cdp-s350bk-pian-digital.jpg", CASIO));
            ProductsDAO.insert(new ProductsDTO(3, ProductType.PIANO, "Korg B2 Black", 2000, 40, "korg-b2-black-pian-digital.jpg", Brand.KORG));
            ProductsDAO.insert(new ProductsDTO(4, ProductType.PIANO, "Korg G1B Air Brown", 6100, 5, "korg-g1-air-br-pian-digital-7829.jpg", Brand.KORG));
            ProductsDAO.insert(new ProductsDTO(5, ProductType.PIANO, "Dexibell VIVO H5 Black", 11100, 6, "dexibell-vivo-h5-black-pian-digital.jpg", Brand.DEXIBELL));
            ProductsDAO.insert(new ProductsDTO(6, ProductType.PIANO, "Dexibell VIVO H1 Black", 8500, 7, "dexibell-vivo-h1-black-pian-digital.jpg", Brand.DEXIBELL));
            ProductsDAO.insert(new ProductsDTO(7, ProductType.GUITAR, "Ibanez PC12MH-OPN", 720, 2, "ibanez-pc12mh-opn-chitara-acustica.jpg", Brand.IBANEZ));
            ProductsDAO.insert(new ProductsDTO(8, ProductType.GUITAR, "Ibanez AW54JR-OPN", 1100, 4, "ibanez-aw54jr-opn-chitara-acustica.jpg", Brand.IBANEZ));
            ProductsDAO.insert(new ProductsDTO(9, ProductType.GUITAR, "Freedom AG-300-BKS", 350, 6, "freedom-ag-300-bks-chitara-acustica.jpg", Brand.FREEDOM));
            ProductsDAO.insert(new ProductsDTO(10, ProductType.GUITAR, "Freedom AG-300-WRS", 450, 10, "freedom-ag-300-wrs-chitara-acustica.jpg", Brand.FREEDOM));
            ProductsDAO.insert(new ProductsDTO(11, ProductType.GUITAR, "Takamine GD10-NS", 1050, 12, "takamine-gd10-ns-chitara-acustica.jpg", Brand.TAKAMINE));
            ProductsDAO.insert(new ProductsDTO(12, ProductType.GUITAR, "Takamine GD30-NAT", 1550, 1, "takamine-gd30-nat-chitara-acustica.jpg", Brand.TAKAMINE));
            ProductsDAO.insert(new ProductsDTO(13, ProductType.DRUMS, "Freedom JWM-02", 380, 15, "freedom-jwm-02-toba-mica-fanfara.jpg", Brand.FREEDOM));
            ProductsDAO.insert(new ProductsDTO(14, ProductType.DRUMS, "Freedom JWM-05", 420, 31, "freedom-jwm-05-toba-mica-fanfara.jpg", Brand.FREEDOM));
            ProductsDAO.insert(new ProductsDTO(15, ProductType.DRUMS, "Tama SM42SH-SCB", 12750, 4, "tama-sm42sh-scb-starclassic-maple-set-toba.jpg", Brand.TAMA));
            ProductsDAO.insert(new ProductsDTO(16, ProductType.DRUMS, "TAMA VP44-ABR Silverstar", 4770, 10, "tama-vp44-silverstar-set-toba.jpg", Brand.TAMA));
            ProductsDAO.insert(new ProductsDTO(17, ProductType.DRUMS, "Vox Telstar Maple", 5700, 3, "-9095.jpg", Brand.VOX));
            ProductsDAO.insert(new ProductsDTO(18, ProductType.DRUMS, "VOX Telstar", 5300, 3, "vox-telstar-set-tobe.jpg", Brand.VOX));
            ProductsDAO.insert(new ProductsDTO(19, ProductType.VIOLIN, "Vox Meister VOS34", 400, 4, "vox-meister-vos34-vioara-3-4-cu-case.jpg", Brand.VOX));
            ProductsDAO.insert(new ProductsDTO(20, ProductType.VIOLIN, "VOX MEISTER VOS44", 420, 6, "vox-meister-vos44-vioara-4-4-cu-case.jpg", Brand.VOX));
            ProductsDAO.insert(new ProductsDTO(21, ProductType.VIOLIN, "Vhienna VO44LINZ", 1050, 5, "vhienna-vo44linz-vioara.jpg", Brand.VHIENNA));
            ProductsDAO.insert(new ProductsDTO(22, ProductType.VIOLIN, "Vhienna VO12LINZ", 1000, 2, "vhienna-vo44linz-vioara.jpg", Brand.VHIENNA));
            ProductsDAO.insert(new ProductsDTO(23, ProductType.VIOLIN, "Antoni \"Student\"", 370, 11, "antoni-student-4-4-vioara-cu-case-7524.jpg", Brand.ANTONI));
            ProductsDAO.insert(new ProductsDTO(24, ProductType.VIOLIN, "Antoni Premiere", 1300, 14, "antoni-premiere-vioar-4-4.jpg", Brand.ANTONI));
            ProductsDAO.insert(new ProductsDTO(25, ProductType.VIOLA, "Yamaha VA 5S 155", 2660, 9, "151315-home_default.jpg", Brand.YAMAHA));
            ProductsDAO.insert(new ProductsDTO(26, ProductType.VIOLA, "Yamaha VA 5S 13", 2740, 7, "151282-home_default.jpg", Brand.YAMAHA));
            ProductsDAO.insert(new ProductsDTO(27, ProductType.VIOLA, "Flame Pro LG106", 498, 9, "130806-home_default.jpg", Brand.FLAME));
            ProductsDAO.insert(new ProductsDTO(28, ProductType.VIOLA, "Flame Pro LM110H", 829, 8, "130796-home_default.jpg", Brand.FLAME));
            ProductsDAO.insert(new ProductsDTO(29, ProductType.VIOLA, "Gewa Pure Violaset", 1190, 2, "150370-home_default.jpg", Brand.GEWA));
            ProductsDAO.insert(new ProductsDTO(30, ProductType.CELLO, "Flame MC760LEQ", 1320, 1, "189824-home_default.jpg", Brand.FLAME));
            ProductsDAO.insert(new ProductsDTO(31, ProductType.CELLO, "Flame MC770LEQ", 1200, 60, "189860-home_default.jpg", Brand.FLAME));
            ProductsDAO.insert(new ProductsDTO(32, ProductType.CELLO, "Yamaha VC 7SG44", 10390, 2, "150017-home_default.jpg", Brand.YAMAHA));
            ProductsDAO.insert(new ProductsDTO(33, ProductType.CELLO, "Yamaha VC 5S44", 5929, 1, "150004-home_default.jpg", Brand.YAMAHA));
        }
    }


    public static void initDatabase() {
        createMissingTables();
        sampleProductList();
    }
}
