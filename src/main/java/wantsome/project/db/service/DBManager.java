package wantsome.project.db.service;

import org.apache.commons.dbcp2.BasicDataSource;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

public class DBManager {
    private static final String DB_FILE = "OnlineStore.db";

    private static final BasicDataSource dataSource; // using connection pool from DBCP

    static { // static init bloc, to set up dataSource
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.sqlite.JDBC");
        dataSource.setUrl("jdbc:sqlite:" + DB_FILE);
        dataSource.setInitialSize(10); // initial/minimum size of pool
        // set recommended custom config for SQLite:
        // - foreign_keys: important, to enable FK support (as FK checking is disabled by default)
        // - date_class: recommended, to store date/time values as TEXT in DB (instead of default as NUMBER, seconds since 1970)
        // - date_string_format: optional, to set a different format to use for storing the date values;
        //   here we use a shorter format, without time part (as we don't need that for any field)
        dataSource.setConnectionProperties("foreign_keys=true;date_class=text;date_string_format=yyyy-MM-dd;");
    }

    /**
     * Allows to change the name of database file, useful for example for tests (to use different db from default one)
     * Note: should call this BEFORE any calls to getConnection()
     */
    public static void setDbFile(String dbFile) {
        System.out.println("Using custom SQLite db file: " + new File(dbFile).getAbsolutePath());
        dataSource.setUrl("jdbc:sqlite:" + dbFile);
    }

    /**
     * Provides a connection to the db, taken from the connection pool.
     */
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}

