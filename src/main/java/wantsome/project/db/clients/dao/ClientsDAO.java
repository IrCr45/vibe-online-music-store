package wantsome.project.db.clients.dao;

import wantsome.project.db.clients.dto.ClientsDTO;
import wantsome.project.db.service.DBManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientsDAO {

    public static List<ClientsDTO> getAll() {

        String sql = "select * from clients\n" +
                "order by id";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<ClientsDTO> clientsList = new ArrayList<>();
            while (rs.next()) {
                clientsList.add(extractClientFromResult(rs));
            }
            return clientsList;
        } catch (SQLException e) {
            throw new RuntimeException("The list of clients could not be loaded" + e.getMessage(), e);
        }
    }

    private static ClientsDTO extractClientFromResult(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        String email = rs.getString("email");
        String password = rs.getString("password");
        String name = rs.getString("name");
        String street = rs.getString("street");
        int streetNumber = rs.getInt("streetNumber");
        String town = rs.getString("town");

        return new ClientsDTO(id, email, password, name, street, streetNumber, town);
    }

    public static Optional<ClientsDTO> load(long id) {

        String sql = "select * from clients \n" +
                "where id=?";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(extractClientFromResult(rs));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading client with id: " + id + e.getMessage(), e);
        }
    }

    public static long getIDByEmailAndPassword(String email, String password) {

        String sql = "select ID from clients\n" +
                "where email = ? and password = ?;";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;
            ps.setString(++idx, email);
            ps.setString(++idx, password);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getLong("id");
                }
            }
            return -1;
        } catch (SQLException e) {
            throw new RuntimeException("Error loading client by email: " + e.getMessage(), e);
        }
    }

    public static void insert(ClientsDTO clientsDTO) {

        String sql = "insert into clients(email,password,name,street,streetNumber,town) values (?,?,?,?,?,?)";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            int idx = 0;

            ps.setString(++idx, clientsDTO.getEmail());
            ps.setString(++idx, clientsDTO.getPassword());
            ps.setString(++idx, clientsDTO.getName());
            ps.setString(++idx, clientsDTO.getStreet());
            ps.setInt(++idx, clientsDTO.getStreetNumber());
            ps.setString(++idx, clientsDTO.getTown());

            int affectedRows = ps.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        clientsDTO.setId(generatedKeys.getLong(1));
                    }
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error saving new client " + e.getMessage(), e);
        }
    }

    public static void updateClientAddressDetails(long id, String street, int streetNumber, String town) {
        String sql = "update clients \n" +
                "set street = ?, streetNumber = ?, town = ? where id = ?;";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;
            ps.setString(++idx, street);
            ps.setInt(++idx, streetNumber);
            ps.setString(++idx, town);
            ps.setLong(++idx, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Could update the address of client with id : " + id + e.getMessage(), e);
        }
    }


}
