package wantsome.project.db.clients.dto;

import java.util.Objects;

public class ClientsDTO {

    private long id;
    private final String email;
    private final String password;
    private final String name;
    private final String street;
    private final int streetNumber;
    private final String town;

    public ClientsDTO(String email, String password, String name, String street, int streetNumber, String town) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
    }

    public ClientsDTO(long id, String email, String password, String name, String street, int streetNumber, String town) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public String getTown() {
        return town;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ClientsDTO{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", streetNumber=" + streetNumber +
                ", town='" + town + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClientsDTO)) return false;
        ClientsDTO that = (ClientsDTO) o;
        return id == that.id && streetNumber == that.streetNumber && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(name, that.name) && Objects.equals(street, that.street) && Objects.equals(town, that.town);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, name, street, streetNumber, town);
    }
}



