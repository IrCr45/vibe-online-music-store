package wantsome.project.db.order.dto;

import wantsome.project.db.products.dto.ProductsDTO;

import java.util.Objects;

public class CartItem {

    private ProductsDTO product;
    private int quantity;

    public CartItem(ProductsDTO product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public ProductsDTO getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setProduct(ProductsDTO product) {
        this.product = product;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "product=" + product +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartItem)) return false;
        CartItem cartItem = (CartItem) o;
        return quantity == cartItem.quantity && Objects.equals(product, cartItem.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, quantity);
    }
}
