package wantsome.project.db.order.dto;

import wantsome.project.db.clients.dto.ClientsDTO;

import java.sql.Date;
import java.util.Objects;

public class OrdersDTO {

    private long id;
    private final ClientsDTO client;
    private final Date fulfill_date;
    private final double total_price;

    public OrdersDTO(ClientsDTO client, Date fulfill_date, double total_price) {
        this.client = client;
        this.fulfill_date = fulfill_date;
        this.total_price = total_price;
    }

    public OrdersDTO(long id, ClientsDTO client, Date fulfill_date, double total_price) {
        this.id = id;
        this.client = client;
        this.fulfill_date = fulfill_date;
        this.total_price = total_price;
    }

    public long getId() {
        return id;
    }

    public ClientsDTO getClient() {
        return client;
    }

    public Date getFulfill_date() {
        return fulfill_date;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "OrdersDTO{" +
                "id=" + id +
                ", client=" + client +
                ", fulfill_date=" + fulfill_date +
                ", total_price=" + total_price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrdersDTO)) return false;
        OrdersDTO ordersDTO = (OrdersDTO) o;
        return id == ordersDTO.id && Double.compare(ordersDTO.total_price, total_price) == 0 && Objects.equals(client, ordersDTO.client) && Objects.equals(fulfill_date, ordersDTO.fulfill_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, fulfill_date, total_price);
    }
}
