package wantsome.project.db.order.dto;

import java.util.Objects;

public class CartDTO {

    private final long product_id;
    private final long order_id;
    private final int quantity;

    public CartDTO(long product_id, long order_id, int quantity) {

        this.product_id = product_id;
        this.quantity = quantity;
        this.order_id = order_id;
    }

    public long getOrder_id() {
        return order_id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "CartDTO{" +
                "product_id=" + product_id +
                ", order_id=" + order_id +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartDTO)) return false;
        CartDTO cartDTO = (CartDTO) o;
        return product_id == cartDTO.product_id && order_id == cartDTO.order_id && quantity == cartDTO.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(product_id, order_id, quantity);
    }
}

