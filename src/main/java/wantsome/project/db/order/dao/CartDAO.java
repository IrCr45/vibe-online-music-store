package wantsome.project.db.order.dao;

import wantsome.project.db.order.dto.CartDTO;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CartDAO {


    public static List<CartDTO> getAll() {

        String sql = "select * from cart";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<CartDTO> cartDTOList = new ArrayList<>();
            while (rs.next()) {
                cartDTOList.add(extractCartItemsFromResult(rs));
            }
            return cartDTOList;
        } catch (SQLException e) {
            throw new RuntimeException("The list of products could not be loaded" + e.getMessage(), e);
        }
    }

    private static CartDTO extractCartItemsFromResult(ResultSet rs) throws SQLException {

        long product_id = rs.getLong("product_id");
        long order_id = rs.getLong("order_id");
        int quantity = rs.getInt("quantity");

        return new CartDTO(product_id, order_id, quantity);
    }


    public static void insert(CartDTO cartDTO) {

        String sql = "insert into cart(product_id,order_id,quantity) values (?,?,?)";

        try (
                Connection connection = DBManager.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;

            ps.setLong(++idx, cartDTO.getProduct_id());
            ps.setLong(++idx, cartDTO.getOrder_id());
            ps.setInt(++idx, cartDTO.getQuantity());
            ps.executeUpdate();

        } catch (
                SQLException e) {
            throw new RuntimeException("Order could not be completed" + e.getMessage(), e);
        }

    }

    public static void delete(long id) {

        String sql = "delete from cart where id = ?";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while trying to cancel order!" + e.getMessage(), e);
        }

    }
}
