package wantsome.project.db.order.dao;

import wantsome.project.db.clients.dao.ClientsDAO;
import wantsome.project.db.order.dto.OrdersDTO;
import wantsome.project.db.service.DBManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrdersDAO {

    public static List<OrdersDTO> getAll() {

        String sql = "select * from orders\n" +
                "order by id";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<OrdersDTO> ordersList = new ArrayList<>();
            while (rs.next()) {
                ordersList.add(extractOrdersFromResult(rs));
            }
            return ordersList;
        } catch (SQLException e) {
            throw new RuntimeException("The list of orders could not be loaded" + e.getMessage(), e);
        }
    }

    private static OrdersDTO extractOrdersFromResult(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        long client_id = rs.getLong("client_id");
        Date fulfill_date = rs.getDate("fulfill_date");
        double total_price = rs.getDouble("total_price");
        return new OrdersDTO(id, ClientsDAO.load(client_id).orElseThrow(), fulfill_date, total_price);
    }

    public static List<Long> loadOrderIdListByClientId(long clientId) {

        String sql = "select id from orders \n" +
                "where client_id=?";

        List<Long> orderIdList = new ArrayList<>();

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, clientId);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    orderIdList.add(rs.getLong("id"));
                }
                return orderIdList;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading order with id: " + clientId + e.getMessage(), e);
        }
    }

    public static void insert(OrdersDTO ordersDTO) {

        String sql = "insert into orders(client_id,fulfill_date,total_price) values (?,?,?)";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            int idx = 0;

            ps.setLong(++idx, ordersDTO.getClient().getId());
            ps.setDate(++idx, ordersDTO.getFulfill_date());
            ps.setDouble(++idx, ordersDTO.getTotal_price());

            int affectedRows = ps.executeUpdate();
            if (affectedRows > 0) {
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        long newId = generatedKeys.getLong(1);
                        ordersDTO.setId(newId);
                    }
                }
            }


        } catch (SQLException e) {
            throw new RuntimeException("Error saving new order " + e.getMessage(), e);
        }
    }
}
