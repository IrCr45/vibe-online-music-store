package wantsome.project.db.products.dao;

import wantsome.project.db.products.dto.Brand;
import wantsome.project.db.products.dto.ProductType;

public class Filters {

    private final Brand brand;
    private final ProductType productType;
    private final double priceMin;
    private final double priceMax;

    public Filters(Brand brand, ProductType productType, double priceMin, double priceMax) {
        this.brand = brand;
        this.productType = productType;
        this.priceMin = priceMin;
        this.priceMax = priceMax;
    }

    public Filters(Brand brand, double priceMin, double priceMax) {
        this(brand, null, priceMin, priceMax);
    }

    public Filters(ProductType productType, double priceMin, double priceMax) {
        this(null, productType, priceMin, priceMax);
    }

    public Filters(double priceMin, double priceMax) {
        this(null, null, priceMin, priceMax);
    }

    public Brand getBrand() {
        return brand;
    }

    public ProductType getProductType() {
        return productType;
    }

    public double getPriceMin() {
        return priceMin;
    }

    public double getPriceMax() {
        return priceMax;
    }
}
