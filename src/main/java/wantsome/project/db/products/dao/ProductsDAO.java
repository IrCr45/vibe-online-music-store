package wantsome.project.db.products.dao;

import wantsome.project.db.products.dto.Brand;
import wantsome.project.db.products.dto.ProductType;
import wantsome.project.db.products.dto.ProductsDTO;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductsDAO {


    public static List<ProductsDTO> getAll() {

        String sql = "select * from products\n" +
                "order by id";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            List<ProductsDTO> productsList = new ArrayList<>();
            while (rs.next()) {
                productsList.add(extractProductFromResult(rs));
            }
            return productsList;
        } catch (SQLException e) {
            throw new RuntimeException("The list of products could not be loaded" + e.getMessage(), e);
        }
    }

    private static ProductsDTO extractProductFromResult(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        ProductType productType = ProductType.valueOf(rs.getString("productType"));
        String description = rs.getString("description");
        double price = rs.getDouble("price");
        int stock = rs.getInt("stock");
        String image_path = rs.getString("image_path");
        Brand brand = Brand.valueOf(rs.getString("brand"));
        return new ProductsDTO(id, productType, description, price, stock, image_path, brand);
    }

    public static List<ProductsDTO> getAllOfType(ProductType productType) {

        String sql = "select * from products\n" +
                "where productType = ?\n" +
                "order by id";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, productType.name());

            return getProductsDTO(ps);

        } catch (SQLException e) {
            throw new RuntimeException("Error loading products: " + e.getMessage(), e);
        }
    }

    private static List<ProductsDTO> getProductsDTO(PreparedStatement ps) throws SQLException {
        List<ProductsDTO> productsDTOList = new ArrayList<>();

        try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                productsDTOList.add(extractProductFromResult(rs));
            }
            return productsDTOList;
        }
    }

    public static List<ProductsDTO> getProductsByNamePattern(String pattern) {
        String sql = "select * from products\n" +
                "where description like '%" +
                pattern +
                "%'";

        List<ProductsDTO> products = new ArrayList<>();
        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                products.add(extractProductFromResult(rs));
            }

            return products;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading products by name pattern" + e.getMessage(), e);
        }
    }

    public static List<ProductsDTO> getAllFiltered(Filters filters) {

        StringBuilder sql = new StringBuilder();
        sql.append("select * from products where price >= ? and price <= ?");
        if (filters.getBrand() != null && filters.getProductType() != null) {
            sql.append(" and brand = ? and productType = ? order by id");
        } else if (filters.getBrand() != null && filters.getProductType() == null) {
            sql.append(" and brand = ? order by id");
        } else if (filters.getProductType() != null && filters.getBrand() == null) {
            sql.append(" and productType = ? order by id");
        }

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql.toString())) {
            if (filters.getBrand() != null && filters.getProductType() != null) {

                int idx = 0;
                ps.setDouble(++idx, filters.getPriceMin());
                ps.setDouble(++idx, filters.getPriceMax());
                ps.setString(++idx, filters.getBrand().name());
                ps.setString(++idx, filters.getProductType().name());

            } else if (filters.getBrand() != null && filters.getProductType() == null) {

                int idx = 0;
                ps.setDouble(++idx, filters.getPriceMin());
                ps.setDouble(++idx, filters.getPriceMax());
                ps.setString(++idx, filters.getBrand().name());

            } else if (filters.getProductType() != null && filters.getBrand() == null) {
                int idx = 0;
                ps.setDouble(++idx, filters.getPriceMin());
                ps.setDouble(++idx, filters.getPriceMax());
                ps.setString(++idx, filters.getProductType().name());

            } else {

                int idx = 0;
                ps.setDouble(++idx, filters.getPriceMin());
                ps.setDouble(++idx, filters.getPriceMax());

            }

            return getProductsDTO(ps);

        } catch (SQLException e) {
            throw new RuntimeException("Error loading products! " + e.getMessage(), e);
        }

    }


    public static Optional<ProductsDTO> load(long id) {

        String sql = "select * from products \n" +
                "where id=?";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(extractProductFromResult(rs));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading product with id: " + id + e.getMessage(), e);
        }
    }

    public static void updateStock(long id, int stock) {
        String sql = "update products set stock = ? where id = ?";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;
            ps.setInt(++idx, stock);
            ps.setLong(++idx, id);
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Error updating stock for product with ID = " + id + e.getMessage(), e);
        }
    }

    public static void insert(ProductsDTO productsDTO) {

        String sql = "insert into products(id,productType,description,price,stock,image_path,brand) values (?,?,?,?,?,?,?)";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;

            ps.setLong(++idx, productsDTO.getId());
            ps.setString(++idx, productsDTO.getProductType().name());
            ps.setString(++idx, productsDTO.getDescription());
            ps.setDouble(++idx, productsDTO.getPrice());
            ps.setInt(++idx, productsDTO.getStock());
            ps.setString(++idx, productsDTO.getImage_path());
            ps.setString(++idx, productsDTO.getBrand().name());
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error saving new product with id = " + productsDTO.getId() + e.getMessage(), e);
        }
    }
}
