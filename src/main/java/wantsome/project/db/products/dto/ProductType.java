package wantsome.project.db.products.dto;

public enum ProductType {

    PIANO("Piano"),
    GUITAR("Guitar"),
    DRUMS("Drums"),
    VIOLIN("Violin"),
    VIOLA("Viola"),
    CELLO("Cello");

    private final String label;

    ProductType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
