package wantsome.project.db.products.dto;

public enum Brand {

    CASIO("Casio"),
    KORG("Korg"),
    YAMAHA("Yamaha"),
    DEXIBELL("Dexibell"),
    IBANEZ("Ibanez"),
    FREEDOM("Freedom"),
    TAKAMINE("Takamine"),
    TAMA("Tama"),
    VOX("Vox"),
    VHIENNA("Vhienna"),
    ANTONI("Antoni"),
    FLAME("Flame"),
    GEWA("Gewa");

    private final String label;

    Brand(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
