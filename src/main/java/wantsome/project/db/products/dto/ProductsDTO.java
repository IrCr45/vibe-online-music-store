package wantsome.project.db.products.dto;

import java.util.Objects;

public class ProductsDTO {

    private final long id;
    private final ProductType productType;
    private final String description;
    private final double price;
    private final int stock;
    private final String image_path;
    private final Brand brand;

    public ProductsDTO(long id, ProductType productType, String description, double price, int stock, String image_path, Brand brand) {
        this.id = id;
        this.productType = productType;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.image_path = image_path;
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public long getId() {
        return id;
    }

    public ProductType getProductType() {
        return productType;
    }

    public double getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }

    public String getImage_path() {
        return image_path;
    }

    public Brand getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "ProductsDTO{" +
                "id=" + id +
                ", productType=" + productType +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", image_path='" + image_path + '\'' +
                ", brand=" + brand +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductsDTO)) return false;
        ProductsDTO that = (ProductsDTO) o;
        return id == that.id && Double.compare(that.price, price) == 0 && stock == that.stock && productType == that.productType && Objects.equals(description, that.description) && Objects.equals(image_path, that.image_path) && brand == that.brand;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productType, description, price, stock, image_path, brand);
    }
}
