package wantsome.project.db.invoice.dto;

import java.sql.Date;
import java.util.Objects;

public class OrderDetailsDTO {

    private final String name;
    private final String email;
    private final String street;
    private final int streetNumber;
    private final String town;
    private final Date fulfill_date;
    private final double total_price;

    public OrderDetailsDTO(String name, String email, String street, int streetNumber, String town, Date fulfill_date, double total_price) {
        this.name = name;
        this.email = email;
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
        this.fulfill_date = fulfill_date;
        this.total_price = total_price;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getStreet() {
        return street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public String getTown() {
        return town;
    }

    public Date getFulfill_date() {
        return fulfill_date;
    }

    public double getTotal_price() {
        return total_price;
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", street='" + street + '\'' +
                ", streetNumber=" + streetNumber +
                ", town='" + town + '\'' +
                ", fulfill_date=" + fulfill_date +
                ", total_price=" + total_price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDetailsDTO)) return false;
        OrderDetailsDTO that = (OrderDetailsDTO) o;
        return streetNumber == that.streetNumber && Double.compare(that.total_price, total_price) == 0 && Objects.equals(name, that.name) && Objects.equals(email, that.email) && Objects.equals(street, that.street) && Objects.equals(town, that.town) && Objects.equals(fulfill_date, that.fulfill_date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, street, streetNumber, town, fulfill_date, total_price);
    }
}
