package wantsome.project.db.invoice.dto;

import java.util.Objects;

public class OrderItemDetailsDTO {

    private final String itemDescription;
    private final int quantity;
    private final double amount;

    public OrderItemDetailsDTO(String itemDescription, int quantity, double amount) {
        this.itemDescription = itemDescription;
        this.quantity = quantity;
        this.amount = amount;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "OrderItemDetailsDTO{" +
                "itemDescription='" + itemDescription + '\'' +
                ", quantity=" + quantity +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderItemDetailsDTO)) return false;
        OrderItemDetailsDTO that = (OrderItemDetailsDTO) o;
        return quantity == that.quantity && Double.compare(that.amount, amount) == 0 && Objects.equals(itemDescription, that.itemDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemDescription, quantity, amount);
    }
}
