package wantsome.project.db.invoice.dao;

import wantsome.project.db.invoice.dto.OrderDetailsDTO;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class OrderDetailsDAO {

    public static Optional<OrderDetailsDTO> getOrderDetails(long orderId, long clientId) {
        String sql = "select distinct c.name,c.email,c.street,c.streetNumber,c.town,o.fulfill_date,o.total_price  from orders o \n" +
                "join clients c on o.client_id = c.id \n" +
                " where o.id = ? and c.id = ?;";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;
            ps.setLong(++idx, orderId);
            ps.setLong(++idx, clientId);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new OrderDetailsDTO(
                            rs.getString("name"),
                            rs.getString("email"),
                            rs.getString("street"),
                            rs.getInt("streetNumber"),
                            rs.getString("town"),
                            rs.getDate("fulfill_date"),
                            rs.getDouble("total_price")
                    ));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("The order details could not be loaded for orderID = " + orderId + e.getMessage(), e);
        }
    }
}
