package wantsome.project.db.invoice.dao;

import wantsome.project.db.invoice.dto.OrderItemDetailsDTO;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderItemDetailsDAO {

    public static List<OrderItemDetailsDTO> getItemsDetails(long orderId, long clientId) {
        String sql = "select p.description , c.quantity , ( p.price * c.quantity) 'amount' from orders o \n" +
                "join cart c on o.id = c.order_id\n" +
                "join products p on c.product_id = p.id \n" +
                "join clients c2 on o.client_id = c2.id \n" +
                "where o.id = ? and c2.id = ?;";

        List<OrderItemDetailsDTO> orderItemDetails = new ArrayList<>();

        try (Connection connection = DBManager.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {

            int idx = 0;

            ps.setLong(++idx, orderId);
            ps.setLong(++idx, clientId);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    orderItemDetails.add(new OrderItemDetailsDTO(
                            rs.getString("description"),
                            rs.getInt("quantity"),
                            rs.getDouble("amount")
                    ));
                }
            }
            return orderItemDetails;
        } catch (SQLException ex) {
            throw new RuntimeException("Items details could not be loaded for ID: " + orderId + ex.getMessage() + ex);
        }
    }
}
