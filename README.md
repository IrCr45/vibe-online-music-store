## Wantsome - Project Online Store

### 1. Description

This is an application meant to simulate an online music store shopping experience

__Supported actions:__

- multiple user sessions; User must Sign Up/Log In at start
- searching products by name patterns and by using filtering options
- add/delete products in/from cart
- access the logged user's order history
- update the user's shipping address

---

### 2. Setup

No setup needed, just start the application. If the database is missing
(like on first startup), it will create a new database (of type SQLite, stored in a local file named 'OnlineStore.db'),
and use it to save future data.

Once started, access it with a web browser at: <http://localhost:8080>

---

### 3. Technical details

__User interface__

-web app (started with OnlineStoreWebApp class)

__Technologies__

- main code is written in Java (version 11)
- it uses [SQLite](https://www.sqlite.org/), a small embedded database, for its persistence, using SQL and JDBC to
  access it from Java code
- it uses [Javalin](https://javalin.io/) micro web framework (which includes an embedded web server, Jetty)
- it uses [Velocity](https://velocity.apache.org/) templating engine, to separate the UI code from Java code; UI code
  consists of basic HTML and CSS code

__Code structure__

- java code is organized in packages by its role, on layers:
  - db - database part, including DTOs and DAOs, as well as the code to init and connect to the db
  ![](doc/OnlineStoreDB.png)
  - ui - code related to the interface/presentation layer
  - root package - the main class for the web app interface it supports

web resources are found in `main/resources` folder:

- under `/public` folder - static resources to be served by the web server directly (images, css files)
- all other (directly under `/resources`) - the Velocity templates

Note: The focus of this project is both on the back-end and on the front-end part.


---

### 4. Future plans

Ideas for future improvement/features:

- import products using CSV files
- creating web pages with more details for each product
- using JavaScript for implementing animations

